﻿namespace Principles.SRP.Refactor
{
    using System.Collections.Generic;

    class Service
    {
        private ProductRepository repository;

        public Service()
        {
            repository = new ProductRepository();
        }

        public double CalculateProductTax(Product product)
        {
            if (product == null) return 0;

            const double Tax = 0.19;
            double productTax = product.Price * Tax;
            return productTax;
        }

        public bool SaveProduct(Product newProduct)
        {
            if (newProduct == null || newProduct.ProductId < 0 || string.IsNullOrEmpty(newProduct.Name))
            {
                return false;
            }

            repository.Save(newProduct);
            return true;
        }

        public List<Product> ListProducts()
        {

            var products = repository.List();
            return products;
        }
    }
}