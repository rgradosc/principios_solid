﻿namespace Principles.ISP.Refactor.Service
{
    using System.Collections.Generic;
    using Model;
    using Repository;

    class ManagerService
    {
        private IProjectRepository dataRepository;

        public ManagerService(IProjectRepository repository)
        {
            dataRepository = repository;
        }

        public bool CreateProject(Project project)
        {
            bool result = true;

            /* business rules */
            dataRepository.CreateProject(project);

            return result;
        }

        public List<Project> GetProjects(int userId)
        {
            /* Validations and business rules */
            var projects = dataRepository.ListProjectsByUser(userId);
            return projects;
        }
    }
}
