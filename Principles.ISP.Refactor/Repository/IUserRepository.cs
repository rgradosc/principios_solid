﻿namespace Principles.ISP.Refactor.Repository
{
    using System.Collections.Generic;
    using Model;

    interface IUserRepository
    {
        void CreateUser(User user);

        List<User> ListUsers();

        void DeleteUser(User user);
    }
}
