﻿namespace Principles.ISP.Refactor.Repository
{
    using System.Collections.Generic;
    using Model;

    interface IProjectRepository
    {
        void CreateProject(Project project);

        List<Project> ListProjectsByUser(int userId);
    }
}
