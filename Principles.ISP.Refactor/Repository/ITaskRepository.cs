﻿namespace Principles.ISP.Refactor.Repository
{
    using System.Collections.Generic;
    using Model;

    interface ITaskRepository
    {
        void CreateTask(Task task);

        List<Task> ListCompletedTasksByUser(int userId);

        void DeleteTask(Task task);
    }
}
