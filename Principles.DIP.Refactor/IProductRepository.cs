﻿namespace Principles.DIP.Refactor
{
    using System.Collections.Generic;

    interface IProductRepository
    {
        void Save(Product newProduct);

        List<Product> List();
    }
}
