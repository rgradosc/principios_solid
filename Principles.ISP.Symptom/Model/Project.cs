﻿namespace Principles.ISP.Symptom.Model
{
    using System;

    class Project
    {
        public int ProjectId { get; set; }

        public string Name { get; set; }
        
        public DateTime StartDate { get; set; }
    }
}
