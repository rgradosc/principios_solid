﻿namespace Principles.ISP.Symptom.Model
{
    using System;

    class User
    {
        public int UserId { get; set; }

        public string Email { get; set; }

        public DateTime CreationDate { get; set; }
    }
}
